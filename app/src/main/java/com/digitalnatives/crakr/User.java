package com.digitalnatives.crakr;

/**
 * Created by alexmcgonagle on 19/05/15.
 */
public class User {

    public String name;
    public String phNumber;
    public byte[] image;
    public double longitude;
    public double latitude;


    public User(byte[] image, String name, String phNumber, double longitude, double latitude) {
        this.image = image;
        this.name = name;
        this.phNumber = phNumber;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhNumber() {
        return phNumber;
    }

    public void setPhNumber(String phNumber) {
        this.phNumber = phNumber;
    }


    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }



}
