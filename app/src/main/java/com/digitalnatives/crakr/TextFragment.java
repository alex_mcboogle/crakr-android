package com.digitalnatives.crakr;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by alexmcgonagle on 21/05/15.
 */
public class TextFragment extends Fragment{

    TextView text;
    TextView vers;

    @Override

    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.text_fragment, container, false);
        text = (TextView) view.findViewById(R.id.name);
        vers = (TextView)view.findViewById(R.id.phoneNumber);
        return view;
    }
}
