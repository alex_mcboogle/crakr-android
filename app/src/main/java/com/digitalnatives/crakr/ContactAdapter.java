package com.digitalnatives.crakr;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Alex McGonagle on 22/04/15.
 * Contact adapter for the main activity
 */
public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.CardViewHolder>{

    List<Contact> contacts;

    ContactAdapter(List<Contact> contacts){
        this.contacts = contacts;
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView name;
        TextView number;
        TextView distance;
        CircleImageView profile;

        CardViewHolder(View itemView) {
            super(itemView);

            //find widgets in layouts
            cv = (CardView)itemView.findViewById(R.id.cv);
            name = (TextView)itemView.findViewById(R.id.name);
            number = (TextView)itemView.findViewById(R.id.number);
            distance = (TextView)itemView.findViewById(R.id.distance);
            profile = (CircleImageView)itemView.findViewById(R.id.circleImage);
        }
     }

    //Use item.xml for layout in RecyclerView
    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_list_item, viewGroup, false);
        CardViewHolder cvh = new CardViewHolder(v);
        return cvh;
    }

    //set title and information for each card in the RecyclerView
    @Override
    public void onBindViewHolder(CardViewHolder cardViewHolder, int i) {
        cardViewHolder.name.setText(contacts.get(i).name);
        cardViewHolder.number.setText(contacts.get(i).number);
        cardViewHolder.distance.setText(contacts.get(i).distance + "km");
        cardViewHolder.profile.setImageBitmap(contacts.get(i).profile);
    }



}


