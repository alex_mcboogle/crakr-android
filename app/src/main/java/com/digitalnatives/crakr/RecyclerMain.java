package com.digitalnatives.crakr;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;


public class RecyclerMain extends AppCompatActivity {

    private List<Contact> contacts;
    private ArrayList<String> locations;
    private RecyclerView rv;
    private String currentUserId;
    private int kmDistanceFrom;
    private double latitude;
    private double longitude;
    Bitmap newBit;
    ProgressDialog progressDialog;
    ParseGeoPoint point;

    private static final String TAG = "CrakrLog";

    ImageButton FAB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_fragment);

        Log.d(TAG, "Online status? -- > " + ParseUser.getCurrentUser().get("online"));

        ParseUser.getCurrentUser().put("online", true);
        ParseUser.getCurrentUser().saveInBackground();

        FAB = (ImageButton) findViewById(R.id.imageButton);

        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listAllUsers();
            }
        });

        //set default distance from user
        kmDistanceFrom = 50;


        //get GPS first
        //get location and upload it to Parse
        //setup GPS
        GPSTracker gps = new GPSTracker(this);
        // check if GPS enabled
        if (gps.canGetLocation()) {
        } else {
            gps.showSettingsAlert();
        }

        locations = new ArrayList<>();
        contacts = new ArrayList<>();
        currentUserId = ParseUser.getCurrentUser().getObjectId();

        //setup RecyclerView
        rv = (RecyclerView) findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(false);

        setAndParseLocation();
        listAllUsers();

        //RecyclerView card ontouchlistener.
        rv.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent =  new Intent(view.getContext(), CardInflate.class);
                        //Add data for layout
                        intent.putExtra("name", contacts.get(position).name);
                        intent.putExtra("number", contacts.get(position).number);
                        intent.putExtra("distance", contacts.get(position).distance);
                        intent.putExtra("location", locations.get(position));
                        startActivity(intent);

                    }
                })
        );


        Log.d(TAG, "Current username-->" + ParseUser.getCurrentUser().getUsername());
        Log.d(TAG, "Online status at the end of code? -- > " + ParseUser.getCurrentUser().get("online"));

    }


    //list all users method
    public void listAllUsers() {

        Log.d(TAG, "Session token" + ParseUser.getCurrentUser().getSessionToken());

        //create progress dialog
        progressDialog = ProgressDialog.show(this, "",
                "Loading users", true);
        contacts.clear();
        Log.d(TAG, "LAT = " + latitude);
        Log.d(TAG, "LONG = " + longitude);


        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereNotEqualTo("objectId", currentUserId);
        query.whereWithinKilometers("location", point, kmDistanceFrom);
        query.whereEqualTo("online", true);
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> userLocations, ParseException e) {
                if (e == null) {
                    for (int i = 0; i < userLocations.size(); i++) {

                        Log.d(TAG, "Loop = " + i);
                        //download image and create bitmap
                        ParseFile profileFile = (ParseFile)userLocations.get(i).get("profilePhoto");
                        profileFile.getDataInBackground(new GetDataCallback() {
                            public void done(byte[] data, ParseException e) {
                                if (e == null) {
                                    Log.d(TAG, "Data present");
                                    newBit = BitmapFactory.decodeByteArray(data, 0, data.length);
                                    Log.d(TAG, "Image data length? =" + data.length);

                                } else {
                                    Log.d(TAG, "Parse Error = " + e);
                                    Toast.makeText(getApplicationContext(), "Image Loading Error", Toast.LENGTH_LONG).show();
                                }

                            }

                        });

                        //calculate distance from user to others
                        double distance = point.distanceInKilometersTo(userLocations.get(i).getParseGeoPoint("location"));
                        int distanceKm = (int) distance;
                        String distanceString = Integer.toString(distanceKm);

                        contacts.add(new Contact(userLocations.get(i).getUsername(), userLocations.get(i).get("mobNo").toString(), distanceString, newBit));

                        locations.add(userLocations.get(i).getParseGeoPoint("location").toString());

                        Log.d(TAG, userLocations.get(i).getUsername() + " " + userLocations.get(i).get("mobNo").toString() + " " + distance + "km");
                    }
                    Log.d(TAG, "Set adapter for RV");

                    //recyclerview method
                    ContactAdapter ca = new ContactAdapter(contacts);
                    rv.setAdapter(ca);
                    progressDialog.dismiss();


                } else {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Error loading list");
                    Log.d(TAG, "Parse error = " + e);
                    logout();
                }
            }
        });
    }


    public void setAndParseLocation() {
        latitude = 0;
        longitude = 0;
        GPSTracker gps = new GPSTracker(getApplicationContext());
        latitude = gps.getLatitude();
        longitude = gps.getLongitude();
        point = new ParseGeoPoint(latitude, longitude);
        ParseUser.getCurrentUser().put("location", point);
        ParseUser.getCurrentUser().saveInBackground();
    }


    public void logout() {
        Log.d(TAG, "Logout system / Current user =" + ParseUser.getCurrentUser().getUsername());
        ParseUser.getCurrentUser().put("online", false);
        ParseUser.getCurrentUser().saveInBackground();
        Log.d(TAG, "Online status? -- > " + ParseUser.getCurrentUser().get("online"));
        ParseUser.logOut();
        Intent loadLogin = new Intent(this, Login.class);
        startActivity(loadLogin);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_recycler_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent startSettings = new Intent(this, Settings.class);
                startActivity(startSettings);
                return true;
            case R.id.action_logout:
                logout();
            default:
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        logout();
    }



}
