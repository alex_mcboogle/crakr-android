package com.digitalnatives.crakr;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseFile;
import com.parse.ParseUser;

import java.io.ByteArrayOutputStream;

import de.hdodenhof.circleimageview.CircleImageView;


public class SelectProfilePhoto extends AppCompatActivity {

    private Button skip;
    private Button accept;
    private TextView name;
    private TextView number;
    private String nameStr;
    private String numStr;
    private String pwdStr;

    private int pickerInt;
    private int loginint;

    private CircleImageView pickImage;
    private Intent main;
    private final int REQUEST_CODE = 1;

    private static final String TAG = "CrakrLog";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_profile_photo);

        pickerInt = 0;
        loginint = 0;

        main = new Intent(this, RecyclerMain.class);


        name = (TextView)findViewById(R.id.nameText);
        number = (TextView)findViewById(R.id.numberText);
        pickImage = (CircleImageView) findViewById(R.id.circleImage);
        skip = (Button)findViewById(R.id.skiptBtn);
        accept = (Button)findViewById(R.id.accept);

        //get bundle extras to set name and number
        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            nameStr = extras.getString("name");
            numStr = extras.getString("mob");
            pwdStr = extras.getString("pwd");
            name.setText(nameStr);
            number.setText(numStr);
        }

        if(loginint == 1){
            ParseUser.logInInBackground(nameStr, pwdStr, new LogInCallback() {
                public void done(ParseUser user, com.parse.ParseException e) {
                    if (user != null) {
                        startActivity(main);
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "Wrong username/password combo",
                                Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(main);
            }
        });

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pickerInt == 1){
                    startActivity(main);
                } else {
                    Toast.makeText(getApplicationContext(), "You need to select a picture first", Toast.LENGTH_LONG).show();

                }
            }
        });


        pickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_CODE);
            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == REQUEST_CODE){ if (resultCode == RESULT_OK) {
                    pickerInt = 1;
                    Log.d(TAG, "Photo selected");
                    Uri uri = data.getData();
                    String[]projection = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                    cursor.moveToFirst();
                    //get filepath of Image
                    int columnIndex = cursor.getColumnIndex(projection[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();
                    //create bitmap and drawable
                    Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                    Drawable drawable = new BitmapDrawable(bitmap);
                    //set drawable
                    pickImage.setImageDrawable(drawable);
                    //create byte array for parse
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 40, stream);
                    byte[] image = stream.toByteArray();
                    ParseFile file = new ParseFile("userPhoto", image);
                    //upload photo
                    ParseUser.getCurrentUser().put("profilePhoto", file);
                    ParseUser.getCurrentUser().saveInBackground();
                    Log.d(TAG, "Image Uploaded");
                    loginint = 1;

                }
            }

    }




}






