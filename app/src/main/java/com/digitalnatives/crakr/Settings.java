package com.digitalnatives.crakr;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.parse.ParseFile;
import com.parse.ParseUser;

import java.io.ByteArrayOutputStream;

import de.hdodenhof.circleimageview.CircleImageView;


public class Settings extends AppCompatActivity {

    TextView profilePic;
    TextView displayName;
    CircleImageView picture;
    Bitmap newBit;

    private static final String TAG = "CrakrLog";
    private final int REQUEST_CODE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);



        profilePic = (TextView)findViewById(R.id.changeProfilePic);
        displayName = (TextView)findViewById(R.id.changeDisplayName);
        picture = (CircleImageView)findViewById(R.id.profilePhoto);

        picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_CODE);

            }
        });

        displayName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        //enable back button on activity
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE){
            if (resultCode == RESULT_OK)
            {
                Log.d(TAG, "Photo selected");
                Uri uri = data.getData();
                String[]projection = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(projection[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();

                Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                Drawable drawable = new BitmapDrawable(bitmap);

                picture.setImageDrawable(drawable);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 40, stream);
                byte[] image = stream.toByteArray();
                ParseFile file = new ParseFile("userPhoto", image);


                ParseUser.getCurrentUser().put("profilePhoto", file);
                ParseUser.getCurrentUser().saveInBackground();
                Log.d(TAG, "Image Uploaded");
            }
        }

    }

}
