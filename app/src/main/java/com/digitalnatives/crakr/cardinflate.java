package com.digitalnatives.crakr;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Alex McGonagle on 22/04/15.
 * The cardinflate activity is used to switch layouts when
 * the RecyclerView detects a touch event.
 * An if/else statement cycles through an int position bundled with
 * the intent that launched the activity.
 */
public class CardInflate extends AppCompatActivity {

    private String name;
    private String number;
    private String distance;
    private String location;

    private CircleImageView picture;
    private TextView nameText;
    private TextView numberText;
    private TextView distanceText;
    private TextView geoPointText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardinflate);

        nameText = (TextView)findViewById(R.id.name);
        numberText = (TextView)findViewById(R.id.number);
        distanceText = (TextView)findViewById(R.id.distanceKM);
        geoPointText = (TextView)findViewById(R.id.geoPoint);

        //set actionbar title
        setTitle("Crakr");
        //set card position

        //set actionbar "back" button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //get intent extras
        Bundle extras = getIntent().getExtras();
        //Set cardplace extra to value
        name = extras.getString("name");
        number = extras.getString("number");
        distance = extras.getString("distance");
        location = extras.getString("location");

        nameText.setText(name);
        numberText.setText(number);
        distanceText.setText(distance);
        geoPointText.setText(location);

    }





}
