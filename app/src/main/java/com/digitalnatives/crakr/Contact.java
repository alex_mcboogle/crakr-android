package com.digitalnatives.crakr;

import android.graphics.Bitmap;

/**
 * Created by alexmcgonagle on 29/04/15.
 */
public class Contact{
    String name;
    String number;
    String distance;
    Bitmap profile;

    Contact(String name, String number, String distance, Bitmap profile) {
        this.name = name;
        this.number = number;
        this.distance = distance;
        this.profile = profile;
    }
}
