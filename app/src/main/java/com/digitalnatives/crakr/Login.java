package com.digitalnatives.crakr;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseUser;


public class Login extends AppCompatActivity{

    private Button signUpButton;
    private Button loginButton;
    private EditText usernameField;
    private EditText passwordField;
    private String username;
    private String password;
    private Intent startMainActivity;
    private Intent startSignUp;


    private static final String TAG = "CrakrLog";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //setup buttons and text
        signUpButton = (Button) findViewById(R.id.signupButton);
        loginButton = (Button) findViewById(R.id.loginButton);
        usernameField = (EditText) findViewById(R.id.loginUsername);
        passwordField = (EditText) findViewById(R.id.loginPassword);

        //create RecyclerActivity intent
        startMainActivity = new Intent(this, RecyclerMain.class);

        //set current user
        final ParseUser currentUser = ParseUser.getCurrentUser();

        //check if current user is already logged in
        if (currentUser != null) {
            startActivity(startMainActivity);
        }

        Log.d(TAG, "Current user " + currentUser);


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Login button clicked");
                username = usernameField.getText().toString();
                password = passwordField.getText().toString();
                //send username and password to Parse. If there are no errors then load intent
                ParseUser.logInInBackground(username, password, new LogInCallback() {
                    public void done(ParseUser user, com.parse.ParseException e) {
                        if (user != null) {
                            startActivity(startMainActivity);
                        } else {
                            Toast.makeText(getApplicationContext(),
                                    "Wrong username/password combo",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

        //load sign up activity
        startSignUp = new Intent(this, SignUp.class);

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(startSignUp);
            }
        });


    }

}



