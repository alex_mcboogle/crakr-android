package com.digitalnatives.crakr;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseUser;

/**
 * Created by alexmcgonagle on 02/05/15.
 * This class adds Parse to each activity onCreate
 */
public class ParseInit extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "qiOfoQxlhw6Af2adN1ZgpjpvFvjnssOHsLmC4pFs", "DhxbXZhCiVcQnjqbiHZVb7MJrpgEo1OHwRXhqA4J");
        ParseUser.enableRevocableSessionInBackground();
    }
}
