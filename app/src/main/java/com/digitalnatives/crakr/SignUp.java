package com.digitalnatives.crakr;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseUser;
import com.parse.SignUpCallback;


public class SignUp extends AppCompatActivity {

    private Button signUpButton;
    private String username;
    private String password;
    private String mobileNumber;
    private EditText usernameField;
    private EditText mobilenumberField;
    private EditText passwordField;
    private Intent startPhotoActivity;
    private ProgressDialog progressDialog;



    private static final String TAG = "CrakrLog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        startPhotoActivity = new Intent(this, SelectProfilePhoto.class);

        signUpButton = (Button) findViewById(R.id.signupButton);
        usernameField = (EditText) findViewById(R.id.signupUsername);
        passwordField = (EditText) findViewById(R.id.signupPassword);
        mobilenumberField = (EditText) findViewById(R.id.mobileNumberEdit);

        final ParseUser user = new ParseUser();


        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = ProgressDialog.show(view.getContext(), "",
                        "", true);
                username = usernameField.getText().toString();
                mobileNumber = mobilenumberField.getText().toString();
                password = passwordField.getText().toString();
                //field validation
                if (username.length() < 4 || mobileNumber.length() < 7 || password.length() < 7) {
                    Toast.makeText(SignUp.this, "Username, mobile number or password is too short",
                            Toast.LENGTH_SHORT).show();
                } else {
                    //set user details, add extras to intent and load SelectProfilePhoto
                    user.setUsername(username);
                    startPhotoActivity.putExtra("name", username);
                    user.put("mobNo", mobileNumber);
                    startPhotoActivity.putExtra("mob", mobileNumber);
                    user.setPassword(password);
                    startPhotoActivity.putExtra("pwd", password);
                    user.put("online", true);

                    //Sign user in and check for errors
                    user.signUpInBackground(new SignUpCallback() {
                        public void done(com.parse.ParseException e) {
                            if (e == null) {
                                progressDialog.dismiss();
                                startActivity(startPhotoActivity);
                            } else {
                                Toast.makeText(getApplicationContext(),
                                        "There was an error signing up."
                                        , Toast.LENGTH_LONG).show();
                                progressDialog.dismiss();
                            }
                        }
                    });
                }

            }
        });

    }
}









