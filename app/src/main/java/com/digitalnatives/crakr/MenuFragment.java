package com.digitalnatives.crakr;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexmcgonagle on 21/05/15.
 *
 * Test design for Fragment layout version
 */
public class MenuFragment extends Fragment {

    private List<Contact> contacts;
    private RecyclerView rv;
    private String currentUserId;
    private int kmDistanceFrom;

    private double latitude;
    private double longitude;

    private static final String TAG = "CrakrLog";

    ImageButton FAB;

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState){

        View view =inflater.inflate(R.layout.list_fragment, container, false);

        Log.d(TAG, "Online status? -- > " + ParseUser.getCurrentUser().get("online"));


        FAB = (ImageButton)view.findViewById(R.id.imageButton);

        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listAllUsers();
            }
        });

        //set default distance from user
        kmDistanceFrom = 10;

        //set
        ParseUser.getCurrentUser().put("online", true);
        ParseUser.getCurrentUser().saveInBackground();
        Log.d(TAG, "Online status? -- > " + ParseUser.getCurrentUser().get("online"));

        //get GPS first
        //get location and upload it to Parse
        //setup GPS
        GPSTracker gps = new GPSTracker(getActivity());
        // check if GPS enabled
        if (gps.canGetLocation()) {
        } else {
            gps.showSettingsAlert();
        }
        //set up ParseUser
        contacts = new ArrayList<>();
        currentUserId = ParseUser.getCurrentUser().getObjectId();

        //setup RecyclerView
        rv = (RecyclerView)view.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(false);

        listAllUsers();

//        rv.addOnItemTouchListener(
//                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(View view, int position) {
//                       TextFragment text = (TextFragment)getFragmentManager().findFragmentById(R.id.fragment2);
//
//
//                    }
//                })
//        );


        return view;
    }

//    @Override
//    public void onListItemClick(ListView l, View v, int position, long id) {
//        TextFragment txt = (TextFragment)getFragmentManager().findFragmentById(R.id.fragment2);
//        txt.change(AndroidOS[position],"Version : "+Version[position]);
//        getListView().setSelector(android.R.color.holo_blue_dark);
//    }



    public void listAllUsers() {
        contacts.clear();
        setAndParseLocation();
        ParseGeoPoint point = new ParseGeoPoint(latitude, longitude);
        ParseUser.getCurrentUser().put("location", point);
        ParseUser.getCurrentUser().saveInBackground();
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereWithinKilometers("location", point, kmDistanceFrom);
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> userLocations, ParseException e) {
                if (e == null) {
                    for (int i = 0; i < userLocations.size(); i++) {
                       // contacts.add(new Contact(userLocations.get(i).getUsername().toString(), userLocations.get(i).get("mobNo").toString()));
                        Log.d(TAG, userLocations.get(i).getUsername().toString() + " " + userLocations.get(i).get("mobNo").toString());
                    }
                    Log.d(TAG, "Set adapter for RV");

                    //recyclerview method
                    final ContactAdapter ca = new ContactAdapter(contacts);
                    rv.setAdapter(ca);
                    Log.d(TAG, "Finished setting RVadapter");

                } else {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Error loading list");
                }
            }
        });
    }


    public void setAndParseLocation() {
        latitude = 0;
        longitude = 0;
        GPSTracker gps = new GPSTracker(getActivity());
        latitude = gps.getLatitude();
        longitude = gps.getLongitude();
    }


    public void logout() {
        Log.d(TAG, "Logout system / Current user =" + currentUserId);
        ParseUser.getCurrentUser().put("online", false);
        Log.d(TAG, "Online status? -- > " + ParseUser.getCurrentUser().get("online"));
        ParseUser.getCurrentUser().saveInBackground();
        ParseUser.logOut();
        Log.d(TAG, "Logout system / Current user =" + currentUserId);
        Intent loadLogin = new Intent(getActivity(), Login.class);
        startActivity(loadLogin);
    }


}
